 = Basics =

Provides firefox integration for the youtube-dl script and provides limited 
feedback on the ongoing download. Includes a button to list all ongoing 
downloads (see view->toolbars->customize).

It is possible to change the default destination of downloads, audio extraction 
and format conversions (see Tools->Addons).

 = Requirements =

 * Unix				(only got ubuntu to test on)
 * A shell			(youtube-dl is called using exec)  
 * Python 2.7.2+ 	(on the path)
 * ffmpeg			(on the path)
 * FireFox 10+	

If it doesn't work, it probably goes wrong when trying to execute the 
youtube-dl script. Make sure the original script is can be executed using
./youtube-dl from the terminal. For more feedback, start firefox from the 
terminal. The standard output from youtube-dl will be visible in the 
terminal.

 = Future =

This project is currently scratching my personal itch. Feel free to contribute
if you want yours scratched too. Now maybe you have an itch and you don't know
it yet. So here are some suggestions improvements.

 * 	Buttons on other sites then youtube. The youtube-dl script provides support
	for a large number of websites. However currently this extensions only adds
	a button to youtube. Value: 0.2 versions.
	
	Adding new buttons is simple. You can write a grease monkey script to 
	insert the button, convert it to a extension using the User Script Compiler
	and inserting the relevant bit from the generated script-compiler.js into 
	this script compiler. 
	
	Communication between the privileged xul content and local script is done
	through events. Events are launched from an element with id=download-button.
	This element should also have the attributes download-url, download-title, 
	download-status. Two events are supported download-button-interval and 
	download-button-download. Both will set the download-button-status property
	on the element.
	
	Value: 0.2 versions.
	
 * 	Buttons on other places on youtube. The youtube-dl scripts provides support
	for downloading videos from playlists, searches and home pages. Value: 0.2 
	versions. 
	
	This may need some structural changes to support to pass parameters with the 
	number of videos that will be downloaded. Value: 0.2 versions.

 * 	Progress reports. Currently limited information is displayed on the video
 	page and there is an optional button to list all ongoing downloads. But the 
 	progress of the download and conversion can not be communicated. This will
 	require some changes to the youtube-dl script. Value: 0.2 versions.
 	
 *	Download management. Support for canceling and resuming downloads would make
 	the extension more user friendly. Value: 0.1 versions.
 	
 	
== Licence ==

GPLV3 for everything but youtube-dl which public domain.