function Download(url, title, process) {

	var NOT_STARTED = 'Download';
	var STARTED = 'Downloading';
	var FINISHED = 'Finished';
	var FAILED = 'Failed';

	var status = NOT_STARTED;

	this.start = function(args) {
		status = STARTED;
		process.runAsync(args, args.length, this);
	}
	
	this.stop = function() {
		status = FAILED;
		process.kill();
	}

	this.getURL = function() {
		return url;
	}

	this.getStatus = function() {
		return status;
	}

	this.getTitle = function() {
		return title;
	}

	/**
	 * Observes the download process.
	 */
	this.observe = function(subject, topic, data) {

		if (topic == "process-finished") {
			// alert("subject: " + subject + " topic: " + topic + " data: " +
			// data +" exit-value: "+ subject.exitValue );
			status = FINISHED;

		} else if (topic == "process-failed") {
			// alert("subject: " + subject + " topic: " + topic + " data: " +
			// data +" exit-value: "+ subject.exitValue );

			status = FAILED;
		}

	}

}

var downloadButton = {

	downloads : new Array(),

	onLoad : function() {

		// Locate the youtube-dl script. It is stored in the xpi.
		const
		ID = 'downloadbutton@lukehoser.com';
		Components.utils.import("resource://gre/modules/AddonManager.jsm");
		AddonManager.getAddonByID(ID, function(addon) {
			downloadButton.youtubeDLURI = addon
					.getResourceURI("/content/youtube-dl")
		});

		// Locate our preferences
		downloadButton.prefs = Components.classes["@mozilla.org/preferences-service;1"]
				.getService(Components.interfaces.nsIPrefService).getBranch(
						"extensions.downloadbutton.");

		this.initialized = true;

	},

	onUnLoad : function() {
		window.removeEventListener("load", downloadButton.onLoad);
		window.removeEventListener('unload', downloadButton.onUnLoad);
		document.removeEventListener("download-button-clicked",
				downloadButton.download, false, true);
	},



	/**
	 * @return the download associated with the given url or null if there is no
	 *         such download.
	 */
	getDownload : function(url) {

		for ( var i = 0; i < downloadButton.downloads.length; i++) {
			var download = downloadButton.downloads[i];
			if (download.getURL() == url) {
				return download;
			}
		}

		return null;
	},

	/**
	 * Event listener for the download-button-interval event.
	 * 
	 * Updates the status of the download button by setting the download-status
	 * attribute.
	 * 
	 * The java script caller can then update the button in the proper fashion.
	 */
	interval : function(event) {

		// Extract url so we can find associated download.
		var button = event.target;
		var url = button.getAttribute("download-url");
		var download = downloadButton.getDownload(url);

		if (download) {
			button.setAttribute('download-status', download.getStatus())
		} else {
			button.setAttribute('download-status', 'Download') //NOT_STARTED
		}

	},
	/**
	 * Event listener for the download-button-download event.
	 * 
	 * Starts the download when the download button is pressed.
	 * 
	 * The url to download is taken from buttons download-url property. The
	 * download-title property is used for displaying.
	 * 
	 */
	download : function(event) {

		var url = event.target.getAttribute('download-url');
		var title = event.target.getAttribute('download-title');
		var download = downloadButton.getDownload(url);


		if (download) {
			download.stop();	
		} else {
			download = downloadButton.buildDownload(url, title);
			downloadButton.downloads.push(download);			
		}
		
		download.start(downloadButton.buildArgs(url));
		
		downloadButton.interval(event);
	},

	buildDownload : function(url, title) {
		// Setup the process that will execute the youtube-dl script.
		var youtubeDLFile = Components.classes["@mozilla.org/file/local;1"]
				.createInstance(Components.interfaces.nsILocalFile);
		youtubeDLFile.initWithPath(downloadButton.youtubeDLURI.path);

		var process = Components.classes["@mozilla.org/process/util;1"]
				.createInstance(Components.interfaces.nsIProcess);
		process.init(youtubeDLFile);

		// Start download
		return new Download(url, title, process);
	},
	

	/**
	 * Builds the arguments for the youtube-dl script.
	 * 
	 * Arguments are based on the settings.
	 * 
	 */
	buildArgs : function(url) {

		var args = [ '--output', downloadButton.buildOutputFormat() ];

		if (downloadButton.prefs.getBoolPref('simulate')) {
			args.push('--simulate');
		}

		if (downloadButton.prefs.getBoolPref('extract-audio')) {
			args.push('--extract-audio');

			format = this.prefs.getCharPref('audio-format')
			args.push('--audio-format');
			args.push(format);
		}

		args.push(url);
		return args;
	},

	/**
	 * Builds an output format regex. Includes both the path in which the file
	 * will be downloaded as well as how the file name will be formatted based
	 * on the information provided on the video information page.
	 */
	buildOutputFormat : function() {

		// get Desktop
		var directory = downloadButton.prefs.getCharPref('directory');
		var file = null;
		if (!directory) {
			// Work around for not being able to store a platform independent
			// default path. Defaults to desktop.
			file = Components.classes["@mozilla.org/file/directory_service;1"]
					.getService(Components.interfaces.nsIProperties).get(
							"Desk", Components.interfaces.nsIFile);
		} else {
			file = Components.classes["@mozilla.org/file/local;1"]
					.createInstance(Components.interfaces.nsILocalFile);
			file.initWithPath(directory);
		}
		// Work around for having to build a regex without access to platform
		// specific directory separators
		file.append("%(title)s.%(ext)s");
		return file.path;
	},


	displayStatus : function(menupopup) {

		// empty all menuitems
		while (menupopup.firstChild != null) {
			menupopup.removeChild(menupopup.firstChild);
		}

		if (downloadButton.downloads.length == 0) {
			var activeDLLabel = document.createElement('menuitem');
			activeDLLabel.setAttribute('label', "No active downloads");
			menupopup.insertBefore(activeDLLabel, menupopup.firstChild);
		}

		for ( var i = 0; i < downloadButton.downloads.length; i++) {
			var activeDLLabel = document.createElement('menuitem');
			var download = downloadButton.downloads[i];
			activeDLLabel.setAttribute('label', download.getStatus() + ' : '
					+ download.getTitle());
			activeDLLabel.setAttribute('oncommand', "downloadButton.openTab('"
					+ download.getURL() + "');");
			menupopup.insertBefore(activeDLLabel, menupopup.firstChild);
		}

	},

	/**
	 * Utility function for the extension button.
	 * 
	 * Opens the tab for the url. If no tab is open for that url a new one is
	 * opened.
	 * 
	 */
	openTab : function(url) {
		var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
				.getService(Components.interfaces.nsIWindowMediator);
		var browserEnumerator = wm.getEnumerator("navigator:browser");

		// Check each browser instance for our URL
		var found = false;
		while (!found && browserEnumerator.hasMoreElements()) {
			var browserWin = browserEnumerator.getNext();
			var tabbrowser = browserWin.gBrowser;

			// Check each tab of this browser instance
			var numTabs = tabbrowser.browsers.length;
			for ( var index = 0; index < numTabs; index++) {
				var currentBrowser = tabbrowser.getBrowserAtIndex(index);
				if (url == currentBrowser.currentURI.spec) {

					// The URL is already opened. Select this tab.
					tabbrowser.selectedTab = tabbrowser.tabContainer.childNodes[index];

					// Focus *this* browser-window
					browserWin.focus();

					found = true;
					break;
				}
			}
		}

		// Our URL isn't open. Open it now.
		if (!found) {
			var recentWindow = wm.getMostRecentWindow("navigator:browser");
			if (recentWindow) {
				// Use an existing browser window
				recentWindow.delayedOpenTab(url, null, null, null, null);
			} else {
				// No browser windows are open, so open a new one.
				window.open(url);
			}
		}
	},

};

window.addEventListener("load", downloadButton.onLoad);
window.addEventListener('unLoad', downloadButton.onUnLoad);

document.addEventListener("download-button-download", downloadButton.download,
		false, true);
document.addEventListener("download-button-interval", downloadButton.interval,
		false, true);