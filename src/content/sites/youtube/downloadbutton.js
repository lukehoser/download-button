// ==UserScript==
// @name           Youtube Download Button
// @description    Adds a button to download and convert youtube videos.
// @namespace      http://googlesystem.blogspot.com
// @include        http://*.youtube.com/watch?*
// @include        https://*.youtube.com/watch?*
// @include        http://youtube.com/watch?*
// @include        https://youtube.com/watch?*
// @version        0.8
// ==/UserScript==


function onDownloadButtonClicked() {
	var evt = document.createEvent('Events');
	evt.initEvent('download-button-download', true, false);
	var element = document.getElementById('download-button');
	element.dispatchEvent(evt);
	
	setStatus();
};

function onDownloadButtonInterval() {
	
	// Dispach event to set status on download-button element.
	var evt = document.createEvent('Events');
	evt.initEvent('download-button-interval', true, false);
	var element = document.getElementById('download-button');
	element.dispatchEvent(evt);
	
	setStatus();
}

function setStatus(){
	var NOT_STARTED = 'Download';
	var STARTED = 'Downloading';
	var FINISHED = 'Finished';
	var FAILED = 'Failed';
	
	// Set status on button
	var event = document.getElementById('download-button');
	var status = event.getAttribute('download-status');
	if (status == NOT_STARTED) {
		button = document.getElementById('download-button-button');
		button.disabled = false;
		button.title = 'Download and convert this video';
		content = document.getElementById('download-button-button-content');
		content.textContent = 'Download';
	} else if (status == STARTED) {
		button = document.getElementById('download-button-button');
		button.disabled = true;
		button.title = 'The video is being downloaded';
		content = document.getElementById('download-button-button-content');
		content.textContent = 'Downloading';
	} else if (status == FINISHED) {
		button = document.getElementById('download-button-button');
		button.disabled = false;
		button.title = 'The video has been downloaded. It can be downloaded again';
		content = document.getElementById('download-button-button-content');
		content.textContent = 'Download (again)';
	} else if (status == FAILED) {
		button = document.getElementById('download-button-button');
		button.disabled = false;
		button.title = 'Downloading the video has failed';
		content = document.getElementById('download-button-button-content');
		content.textContent = 'Download (failed, try again?)';
	}
};

// The button calls the script to dispatch an a download-button-clicked event
// from it. The button
// contains a document-url and document title attribute. These are passed to the
// downloader.
var watchActions = document.getElementById('watch-actions');

if (watchActions) {
	// Add the scripts. Need them added because they're used to later on.
	var script = document.createElement('script');
	script.innerHTML = onDownloadButtonClicked + onDownloadButtonInterval + setStatus;
	document.getElementsByTagName('head')[0].appendChild(script);

	// Add the event. All communication is done through this element
	var event = document.createElement('div');
	event.id = 'download-button';
	event.setAttribute('download-title', document.title);
	event.setAttribute('download-url', document.location);
	document.body.appendChild(event);

	// Add the download video button.
	var button = document.createElement('button');
	button.id = 'download-button-button'
	button.className = 'yt-uix-button ' + 'yt-uix-button-default '
			+ 'yt-uix-tooltip';
	button.setAttribute('onclick', 'onDownloadButtonClicked();');
	button.setAttribute('role', 'button');
	button.title = 'Download and convert this video';
	watchActions.appendChild(button);

	var content = document.createElement('span');
	content.id = 'download-button-button-content'
	content.className = 'yt-uix-button-content';
	content.textContent = 'Download';
	button.appendChild(content);

	// First update
	onDownloadButtonInterval();
	// Every 5 seconds afterwards
	setInterval('onDownloadButtonInterval()', 5000)
}
